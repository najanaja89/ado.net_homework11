﻿using ado.net_homework11.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework11.DataContext
{
    public class DataInitialization : DropCreateDatabaseIfModelChanges<ShopContext>
    {
		//public override void InitializeDatabase(ShopContext context)
  //      {
  //          context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
  //              , string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

  //          base.InitializeDatabase(context);
  //      }
		
        protected override void Seed(ShopContext context)
        {
            ShoppingCart shoppingCart = new ShoppingCart();

            User user = new User
            {
                Login = "najanaja",
                Password = "root",
                ShoppingCartId = shoppingCart.Id
            };

            shoppingCart.UserId = user.Id;
            context.Products.AddRange(new List<Product>
            {
               new Product { Name= "product1", Price = 12000},
               new Product { Name= "product2", Price = 20000},
               new Product { Name= "product3", Price = 300},
               new Product { Name= "product4", Price = 2500},
               new Product { Name= "product5", Price = 350},
            });

            context.ShoppingCarts.Add(shoppingCart);
            context.Users.Add(user);
            context.SaveChanges();
        }

    }
}
