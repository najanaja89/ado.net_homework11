﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework11.Models
{
    public abstract class GuidGenerator
    {
        //[Key]
        public Guid Id { get; set; }
        public GuidGenerator()
        {
            Id = Guid.NewGuid();
        }
    }
}
