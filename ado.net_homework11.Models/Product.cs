﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework11.Models
{
    public class Product : GuidGenerator
    {
        public string Name { get; set; }
        public double Price { get; set; }

        //public Store Store { get; set; }
    }
}
