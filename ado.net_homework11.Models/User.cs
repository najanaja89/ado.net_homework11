﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ado.net_homework11.Models
{
    public class User : GuidGenerator
    {
        public string Login { get; set; }
        public string Password { get; set; }
        [Key, ForeignKey("ShoppingCart")]
        public Guid ShoppingCartId { get; set; }
        public virtual ShoppingCart ShoppingCart { get; set; }
    }
}
