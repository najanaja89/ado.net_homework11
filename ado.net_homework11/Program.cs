﻿using ado.net_homework11.DataContext;
using ado.net_homework11.Models;
using ado.net_homework11.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework11
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new ShopContext())
            {
                context.SaveChanges();
            }

            using (var context = new ShopContext())
            {
                ShopServices shopService = new ShopServices();
                UserRegistration userRegistration = new UserRegistration();

                //Console.WriteLine(shopService.UserExist("najanaja"));
                while (true)
                {
                    string menu;
                    Console.WriteLine("Press 1 to Register");
                    Console.WriteLine("Press 2 to Log in");
                    Console.WriteLine("Press 0 to exit");

                    menu = Console.ReadLine();

                    switch (menu)
                    {
                        case "1":
                            userRegistration.ValidationCheck();
                            while (true)
                            {
                                if (!shopService.UserExist(userRegistration.ExportUser().Login))
                                {
                                    context.Users.Add(userRegistration.ExportUser());
                                    context.ShoppingCarts.Add(userRegistration.ExportShoppingCart());
                                    context.SaveChanges();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("User Exist");
                                    break;
                                }
                            }

                            break;

                        case "2":
                            while (true)
                            {
                                string login = "";
                                Console.WriteLine("Enter login");
                                login = Console.ReadLine().ToLower();
                                if (!shopService.UserExist(login))
                                {
                                    Console.WriteLine("Users not exist!");
                                    break;
                                }
                                else
                                {
                                    string password = "";
                                    Console.WriteLine("Enter password");
                                    password = UserRegistration.ReadPassword();
                                    if (context.Users.Any(user => user.Password == password))
                                    {
                                        while (true)
                                        {
                                            string subMenu = "";
                                            var user = shopService.GetUserByLogin(login);
                                            var cart = shopService.GetShoppingCartByUser(user);
                                            Console.WriteLine("Press 1 to view list of all products");
                                            Console.WriteLine("Press 2 to add products to cart");
                                            Console.WriteLine("Press 3 to view list of products in shopping cart");
                                            subMenu = Console.ReadLine();
                                            switch (subMenu)
                                            {
                                                case "1":
                                                    Console.WriteLine();
                                                    foreach (var product in context.Products.ToList().OrderBy(price => price.Price))
                                                    {
                                                        Console.WriteLine("----------------------------------------------------------------");
                                                        Console.WriteLine($"Product name: {product.Name} \tProduct price: {product.Price}");
                                                    }
                                                    Console.WriteLine();
                                                    break;

                                                case "2":

                                                    shopService.AddProductToCart(cart);
                                                    break;

                                                case "3":
                                                    shopService.ViewCartPositions(cart);
                                                    break;

                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Password Incorrect!");
                                        break;
                                    }
                                }
                            }
                            break;

                        case "0":
                            System.Environment.Exit(0);
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }
}
